!------------------------------------------------------------------------------
!                  GEOS-Chem Global Chemical Transport Model                  !
!------------------------------------------------------------------------------
!BOP
!
! !PROGRAM: main.F90 
!
! !DESCRIPTION: Driver program for the registry example.  
!\\
!\\
! !INTERFACE:
!
PROGRAM Main
!
! !USES:
!
  USE Precision_Mod
  USE Mod1

  IMPLICIT NONE
!
! !REMARKS:
!  Shows how you can obtain pointers to variables in mod1.F90 (or even sub-
!  sets of those variables) by name lookup.
!
! !REVISION HISTORY:
!  06 Jan 2015 - R. Yantosca - Initial version
!EOP
!------------------------------------------------------------------------------
!BOC
!
! !LOCAL VARIABLES:
!
  ! Scalars
  LOGICAL, PARAMETER :: am_I_Root = .TRUE.
  INTEGER            :: RC

  ! Data source pointers
  REAL(fp), POINTER :: datasrce_u10m(:,:  )
  REAL(fp), POINTER :: datasrce_v10m(:,:  )
  REAL(fp), POINTER :: datasrce_tmpu(:,:,:)
  REAL(fp), POINTER :: datasrce_sphu(:,:,:)
  REAL(fp), POINTER :: datasrce_spc_no(:,:,:)
  REAL(fp), POINTER :: datasrce_spc_o3(:,:,:)
  REAL(fp), POINTER :: datasrce_spc_co(:,:,:)
  
   ! Initialize arrays of mod1
  CALL Mod1_Init( am_I_Root, RC )

  ! Lookup pointers by name for State_Met-like fields
  ! MOD1_LOOKUP is a wrapper for REGISTRY_LOOKUP, with StateName="MOD1"
  CALL Mod1_Lookup( am_I_Root, 'U10M', Ptr2D=DataSrce_U10M, RC=RC )
  CALL Mod1_Lookup( am_I_Root, 'V10M', Ptr2D=DataSrce_V10M, RC=RC )
  CALL Mod1_Lookup( am_I_Root, 'TMPU', Ptr3D=DataSrce_TMPU, RC=RC )
  CALL Mod1_Lookup( am_I_Root, 'SPHU', Ptr3D=DataSrce_SPHU, RC=RC )

  print*, '#### State_Met-like fields'
  print*, 'U10M min, max: ', minval( datasrce_u10m ), maxval( datasrce_u10m )
  print*, 'V10M min, max; ', minval( datasrce_v10m ), maxval( datasrce_v10m )
  print*, 'TMPU min, max; ', minval( datasrce_tmpu ), maxval( datasrce_tmpu )
  print*, 'SPHU min, max; ', minval( datasrce_sphu ), maxval( datasrce_sphu )

  ! Lookup pointers by name for State_Chm-like fields
  ! MOD1_LOOKUP is a wrapper for REGISTRY_LOOKUP, with StateName="MOD1"
  CALL Mod1_Lookup( am_I_Root, 'SPC_NO', Ptr3D=DataSrce_spc_no, RC=RC )
  CALL Mod1_Lookup( am_I_Root, 'SPC_O3', Ptr3D=DataSrce_spc_o3, RC=RC )
  CALL Mod1_Lookup( am_I_Root, 'SPC_CO', Ptr3D=DataSrce_spc_co, RC=RC )

  print*, '#### State_Chm-like fields'
  print*, 'SPC_NO min, max: ', minval(datasrce_spc_no), maxval(datasrce_spc_no)
  print*, 'SPC_O3 min, max: ', minval(datasrce_spc_o3), maxval(datasrce_spc_o3)
  print*, 'SPC_CO min, max: ', minval(datasrce_spc_co), maxval(datasrce_spc_co)

  ! Print the registry
  ! MOD1_PRINTREGISTRY is a wrapper for REGISTRY_PRINT, with StateName="MOD1"
  CALL Mod1_PrintRegistry( am_I_Root, RC )

  ! Free fields of Mod1
  CALL Mod1_Cleanup( am_I_Root, RC )

  ! Free local pointers
  datasrce_u10m   => NULL()
  datasrce_v10m   => NULL()
  datasrce_tmpu   => NULL()
  datasrce_sphu   => NULL()
  datasrce_spc_no => NULL()
  datasrce_spc_o3 => NULL()
  datasrce_spc_co => NULL()

END PROGRAM Main
!EOC
  
